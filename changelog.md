# Change Log

## v.0.0.5
- Unified register and unregister methods to be able to do callbefores, commands, callbacks and responses.
- Now you can run multiple terminals in one window.
- Created a method container, so methods are now called with the created plugin instance.
- New methods change_settings, run_command and echo.
- Architecture cleanup, the library is now smaller and more powerful.
- Removed password and upload built-in features. To can add them back use the code in the examples as a guide.
- Changed main options, added the ajax settings object to allow jsonp and more cool stuff, also added i18n.
- New documentation.
- New Content editable field with blinking cursor, instead of text input field.
- Squashed the scroll to bottom bug. Yay!

## v.0.0.4
- Added the $.register_callbefore() function response type.
- Added the $.tokenize() helper function.

## v.0.0.3
- Added upload functionality, using the upload response type.
- Removed more bugs.

## v.0.0.2
- Major code cleanup, the update_content() private function has been re-written.
- Added $.get_command_option() which returns the value for the property for a command option.
- Added disabled="disabled" to terminal input while loading.
- Added charset enctype autofocus to the available Ptty options.
- Added the output value to the quiet response format property.

## v.0.0.1
- The scroll to bottom animation lags if several commands are sent rapidly.
- The history feature for subroutines was broken.
- Terminal width expanded with extremely long commands.
